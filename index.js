(function init() {
    var isTouchDevice = 'ontouchstart' in document.documentElement;

    var switchWrapper = document.getElementById('switch');
    if (isTouchDevice) {
        switchWrapper.addEventListener('touchstart', handleSwitch);
    } else {
        switchWrapper.addEventListener('mousedown', handleSwitch);
    }

    handleSwitch();

    particlesJS.load('particles', 'particlesjs-config.json');
})();

function handleSwitch() {
    var colorSwitch = document.getElementById('color-switch');
    var particles = document.getElementById('particles');

    if (colorSwitch.style.display === "none") {
        colorSwitch.style.display = "block";
        particles.style.display = "none";
    } else {
        colorSwitch.style.display = "none";
        particles.style.display = "block";
    }
}
